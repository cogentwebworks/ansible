## small alpine-ansible container but still have a working s6-overlay process and socklog-overlay


[![Docker Pulls](https://img.shields.io/docker/pulls/cogentwebs/ansible.svg)](https://hub.docker.com/r/cogentwebs/ansible/)
[![Docker Stars](https://img.shields.io/docker/stars/cogentwebs/ansible.svg)](https://hub.docker.com/r/cogentwebs/ansible/)
[![Docker Build](https://img.shields.io/docker/automated/cogentwebs/ansible.svg)](https://hub.docker.com/r/cogentwebs/ansible/)
[![Docker Build Status](https://img.shields.io/docker/build/cogentwebs/ansible.svg)](https://hub.docker.com/r/cogentwebs/ansible/)

This is a very small alpine-ansible container but still have a working s6-overlay process and socklog-overlay .